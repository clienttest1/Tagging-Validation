#Need to install requests package for python
#easy_install requests
import requests
import json
import os
import sys

def main():
    
    ### Get System ID. We will need to variabilie app name too (in this case it's ria_onboarding. We still hard coding here.)
    ### When variabilize app name, we will get actual app name from gitlab-ci.yaml with sed command
    
    ### Get APM ID tag from Gitlab Pipeline
    apm_id = "INC0010002"
    cost_center = "12345"
    #apm_id = os.environ.get("apm_id")
    #cost_center = os.environ.get("cost_center")

    ### Calling SNOW CI
    url_apmid = f"https://dev134570.service-now.com/api/now/table/incident?sysparm_query=number%3D{apm_id}&sysparm_fields=number&sysparm_liit=1"
    url_costcenter = f"https://dev134570.service-now.com/api/now/table/incident?sysparm_query=short_description%3D{cost_center}&sysparm_fields=short_description&sysparm_limit=1"
    
    ### Eg. User name="admin", Password="admin" for this code sample.
    user = os.environ["user"]
    pwd = os.environ["pwd"]
    print(user)
    print(pwd)
    
    ### Set proper headers
    headers = {"Content-Type":"application/json","Accept":"application/json"}

    ### Do the HTTP request
    response_apmid = requests.get(url_apmid, auth=(user, pwd), headers=headers )
    response_costcenter = requests.get(url_costcenter, auth=(user, pwd), headers=headers )

    ### Check for HTTP codes other than 200
    if response_apmid.status_code and response_costcenter.status_code != 200: 
        print('Status:', response_apmid.status_code, 'Headers:', response_apmid.headers, 'Error Response:',response_apmid.json())
        print('Status:', response_costcenter.status_code, 'Headers:', response_costcenter.headers, 'Error Response:',response_costcenter.json())
        exit()

    ### Decode the JSON response into a dictionary and use the data
    json.dumps("response_apmid.json")
    data_apmid = response_apmid.json()
    
    json.dumps("response_costcenter.json")
    data_costcenter = response_costcenter.json()
    
    try:
        print("Valid APM ID tag: " + str(data_apmid["result"][0]["number"]))
    except:
        print("Invalid APM ID tag: " + str(apm_id))
        sys.exit("===>Exit python")

    try: 
        print("Valid Cost Center tag: " + str(data_costcenter["result"][0]["short_description"]))
    except:
        print("Invalid Cost Center tag: " + str(cost_center))
        sys.exit("===>Exit python")

    print("Tag validation completed")
    
if __name__ == "__main__":
    main()
